
from pybind_demo.class_demo import Node, Expander, SearchExpander


class AStarNode(object):

    def __init__(self):
        pass

    def summation(self, data_list):

        value = sum(data_list)

        return value



class AStarExpander(object):

    def __init__(self):
        pass

    def multiply(self, a, b):

        return a * b


astar_node_obj = AStarNode()
astar_expander_obj = AStarExpander()

node_obj = Node(astar_node_obj)
expander_obj = Expander(astar_expander_obj)

search_expander_obj = SearchExpander(node_obj, expander_obj)

data_list = list(range(101))
search_expander_obj.summation(data_list)
search_expander_obj.test(data_list)

search_expander_obj.multiply(1, 2)
