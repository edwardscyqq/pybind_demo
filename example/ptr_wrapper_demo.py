

from pybind_demo.ptr_wrapper_demo import demo_union
from pybind_demo.ptr_wrapper_demo import py_double_ptr

class Grep(object):

    def __init__(self):
        pass

    def py_grep_func(self, cost_ptr: py_double_ptr):
        cost = cost_ptr.get_data()
        cost = cost + 1
        cost_ptr.set_data(cost)


grep_obj = Grep()

demo_union_obj = demo_union(grep_obj)

# test grep_union_func
cost_value = 100
demo_union_obj.grep_union_func(cost_value)
print("python cost_value: ", cost_value)

# test grep_union_func2
demo_union_obj.grep_union_func2()

# test grep_union_func3
demo_union_obj.grep_union_func3()

# test grep_union_func4
demo_union_obj.grep_union_func4()

from pybind_demo.ptr_wrapper_demo import Node
from pybind_demo.ptr_wrapper_demo import Test


class A_Class(object):
    def __init__(self):
        pass

    def foo(self, data):
        data = data + 1

        return data

    def node_func(self, node_obj):
        node_obj.update_value(99.0)
        return node_obj

    def node_func3(self, tuple_data):
        node_obj = tuple_data[0]
        data = tuple_data[1]
        node_obj.update_value(99.0)
        data = 12

        return (node_obj, data)

    def node_func4(self):
        node_obj = Node()
        node_obj.update_value(99.0)
        return node_obj


a_obj = A_Class()

test_obj = Test(a_obj)
test_obj.test_func()


for i in list(range(99)):
    print(i)
    test_obj.test_node_func(i)

test_obj.test_node_func2()

test_obj.test_node_func3()

test_obj.test_node_func4()
