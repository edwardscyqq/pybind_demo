
import setuptools

package_name = "pybind_demo"

setuptools.setup(
    name= package_name,
    version="1.0.0",
    author='Chaoyu Shi',
    author_email='edwardscyqq@gmail.com',
    description="pybind_demo",
    packages=['pybind_demo'],
    zip_safe=False
)

