//
// Created by ed on 12/16/21.
//

#include "python.hpp"



using search_callback = std::function<int(std::vector<float>& )>;


class Search
{
public:
    Search(std::vector<float>& data) {
        data_vector.assign(data.begin(), data.end());
    }
    ~Search() {}

public:
    void test(search_callback search_handle){

        if(search_handle != nullptr){
            int r = search_handle(data_vector);

            std::cout << "result: " << r << std::endl;
        }

    }

private:
    std::vector<float> data_vector;

};




PYBIND11_MODULE(functional_demo, m) {

    py::class_<Search>(m, "Search")
            .def(py::init<std::vector<float>&>())

            .def("test", &Search::test, "test")

            ;

}


