//
// Created by ed on 12/13/21.
//

#include "python.hpp"

int add(int i, int j){
    return i + j;
}

template<typename A, typename B, typename C>
C test(A i, B j){
    return i*j;
}



PYBIND11_MODULE(pybind_demo, m) {


    m.def("add", &add, "add 2 value");

    m.def("test", &test<double, double, double>);
    m.def("test", &test<int, int, int>);



}

