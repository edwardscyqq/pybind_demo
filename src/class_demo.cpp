//
// Created by ed on 12/19/21.
//

#include "python.hpp"


class Node{
public:
    Node(py::object a){
        py_a = a;

    }
    ~Node() {}

public:
    void summation(std::vector<int> v){
        std::cout << "Node summation value: " << py_a.attr("summation")(v) << std::endl;
    }

    int get_node_size(std::vector<int> v){

        node_size = v.size();

        return node_size;
    }

public:
    py::object py_a;

    int node_size;

};


class Expander{
public:
    Expander(py::object a){
        py_a = a;
    }
    ~Expander() {}

public:
    void multiply(int data1, int data2){
        std::cout << "Expander multiply: " << py_a.attr("multiply")(data1, data2) << std::endl;
    }

public:
    py::object py_a;

};


template<class T_NODE,
         class E>
class SearchExpander{
public:
    SearchExpander(T_NODE* node, E* expander)
    : node_(node), expander_(expander)
    {

    }
    ~SearchExpander(){}

public:
    void summation(std::vector<int> v){
        node_->summation(v);
    }

    void multiply(int a, int b){
        expander_->multiply(a, b);
    }

    void test(std::vector<int> v){
        std::cout << "get_node_size: " << node_->get_node_size(v) << std::endl;
    }


private:
    E * expander_;
    T_NODE* node_;


};



PYBIND11_MODULE(class_demo, m) {

    py::class_<Node>(m, "Node")
            .def(py::init<py::object>())

            .def("summation", &Node::summation, "summation")
            ;

    py::class_<Expander>(m, "Expander")
            .def(py::init<py::object>())

            .def("multiply", &Expander::multiply, "multiply")
            ;

    py::class_<SearchExpander<Node, Expander>>(m, "SearchExpander")
            .def(py::init<Node*, Expander* >())

            .def("summation", &SearchExpander<Node, Expander>::summation, "summation")
            .def("multiply", &SearchExpander<Node, Expander>::multiply, "multiply")
            .def("test", &SearchExpander<Node, Expander>::test, "test")
            ;


}



