//
// Created by ed on 1/15/22.
//


#include "python.hpp"
#include <memory>
#include <iostream>
#include <tuple>
#include <vector>


template <class T> class ptr_wrapper
{
public:
    ptr_wrapper() : ptr(nullptr) {}
    ptr_wrapper(T* ptr) : ptr(ptr) {}
    ptr_wrapper(const ptr_wrapper& other) : ptr(other.ptr) {}
    T& operator* () const { return *ptr; }
    T* operator->() const { return  ptr; }
    T* get() const { return ptr; }
    void destroy() { delete ptr; }
    T& operator[](std::size_t idx) const { return ptr[idx]; }

    void set_ptr(T* p){
        ptr = p;
    }

    void set_data(T data){
        *(ptr) = data;
    }

    T get_data(){
        return *ptr;
    }

private:
    T* ptr;
};


class demo_union {
public:
    demo_union(py::object obj) {
        py_obj = obj;
    }
    ~demo_union() {}
public:
    void grep_union_func(double* cost){
        ptr_wrapper<double> cost_ptr(cost);
        py_obj.attr("py_grep_func")(cost_ptr);

        std::cout << "grep_union_func data: " << *cost << std::endl;

    }

    void grep_union_func2(){
        double tmp = 11;
        double *data;
        data = &tmp;

        ptr_wrapper<double> cost_ptr(data);
        py_obj.attr("py_grep_func")(cost_ptr);

        std::cout << "grep_union_func2 data: " << *data << std::endl;
    }

    void grep_union_func3(){
        double tmp = 15;
        double *data;
        data = &tmp;
        grep_union_func(data);

        std::cout << "grep_union_func3 data: " << *data << std::endl;
    }

    void grep_union_func4(){
        double data = 28;
        grep_union_func(&data);

        std::cout << "grep_union_func4 data: " << data << std::endl;
    }

private:
    py::object py_obj;
};



class Node
{
public:
    Node(){ node_value = 0.0; }
    ~Node(){}

public:
    void update_value(double value){
        node_value = value;
    }
    double get_value(){
        return node_value;
    }

private:
    double node_value;
};


class Test
{
public:
    Test(py::object a) {
        py_a = a;

        for(int i=0; i < 100; i++){
            node_vector.push_back(new Node());
        }

    }
    ~Test() {}

public:
    void test_func(){

        int tmp = 100;
        int* data;
        data = &tmp;

        std::cout << "before: " << *data << "  " << data << std::endl;

        py::object r_obj = py_a.attr("foo")(data);
        int a = py::cast<int>(r_obj);

        data = &a;

        std::cout << "after: " << *data << "  " << data << std::endl;

    }

    void test_node_func(int index){
        int id = index;
        Node* node_obj = node_vector.at(id);

        std::cout << "before: " << node_obj << "  " << node_obj->get_value() << std::endl;

        py::object r_obj = py_a.attr("node_func")(node_obj);
        Node* node = py::cast<Node*>(r_obj);

        std::cout << "after: " << node << "  " << node->get_value() << std::endl;

        if(node == node_vector[id]){
            std::cout << "id: " << id << " True" << std::endl;
        }
        else{
            std::cout << "id: " << id << " False" << std::endl;
        }

        node_vector[id] = node;

        std::cout << "after2: " << node_vector.at(id) << "  " << node_vector.at(id)->get_value() << std::endl;

    }

    void test_node_func2(){
        Node* node_obj = new Node();

        std::cout << "before: " << node_obj << "  " << node_obj->get_value() << std::endl;

        py::object r_obj = py_a.attr("node_func")(node_obj);
        Node* node = py::cast<Node*>(r_obj);

        std::cout << "after: " << node << "  " << node_obj->get_value() << std::endl;
    }

    void test_node_func3(){
        Node* node_obj = new Node();
        double data = 1.0;

        std::cout << "before: " << node_obj << "  " << node_obj->get_value() << std::endl;
        std::cout << "before: " << "data:" << "  " << data << std::endl;

        std::tuple<Node*, double> tuple_data = std::make_tuple(node_obj, data);

        py::object r_obj = py_a.attr("node_func3")(tuple_data);
        std::tuple<Node*, double> tuple_r = py::cast<std::tuple<Node*, double>>(r_obj);


        Node* node_after = std::get<0>(tuple_r);
        double data_after = std::get<1>(tuple_r);

        std::cout << "after: " << node_after << "  " << node_after->get_value() << std::endl;
        std::cout << "after: " << "data_after:" << "  " << data_after << std::endl;

    }

    void test_node_func4(){
        Node* node_obj = new Node();

        std::cout << "before: " << node_obj << "  " << node_obj->get_value() << std::endl;

        py::object r_obj = py_a.attr("node_func4")();
        Node* node = py::cast<Node*>(r_obj);

        std::cout << "after: " << node << "  " << node_obj->get_value() << std::endl;
    }

private:
    py::object py_a;

    std::vector<Node*> node_vector;

};

PYBIND11_MODULE(ptr_wrapper_demo, m)
{

    py::class_<Node>(m, "Node")
            .def(py::init<>())

            .def("update_value", &Node::update_value, "update_value")
            .def("get_value", &Node::get_value, "get_value")
            ;

    py::class_<Test>(m, "Test")
            .def(py::init<py::object>())

            .def("test_func", &Test::test_func, "test_func")
            .def("test_node_func", &Test::test_node_func, "test_node_func")

            .def("test_node_func2", &Test::test_node_func2, "test_node_func2")
            .def("test_node_func3", &Test::test_node_func3, "test_node_func3")

            .def("test_node_func4", &Test::test_node_func4, "test_node_func4")
            ;


    py::class_<demo_union>(m, "demo_union")
            .def(py::init<py::object>())

            .def("grep_union_func", &demo_union::grep_union_func, "grep_union_func")
            .def("grep_union_func2", &demo_union::grep_union_func2, "grep_union_func2")
            .def("grep_union_func3", &demo_union::grep_union_func3, "grep_union_func3")
            .def("grep_union_func4", &demo_union::grep_union_func4, "grep_union_func4")
            ;

    py::class_<ptr_wrapper<double>>(m,"py_double_ptr")
            .def(py::init<>())
            .def(py::init<double* >())

            .def("set_data", &ptr_wrapper<double>::set_data, "set_data")
            .def("get_data", &ptr_wrapper<double>::get_data, "set_data")
            ;

}
